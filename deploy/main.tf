terraform {
  backend "s3" {
    bucket         = "ee-devops-test"
    key            = "recipe-app.tfstate"
    region         = "us-east-2"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-state-tf-lock"
  }
}

provider "aws" {
  region  = "us-east-2"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}